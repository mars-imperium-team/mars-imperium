<?php

/*
 * Compile la balise #URL_DOCUMENT_FICHIER, mais en cherchant bien un id_document dans la boucle (et non pas un id_document_fichier)
 */
function balise_URL_DOCUMENT_FICHIER_dist($p) {
	include_spip('balise/url_');
	
	$_id = interprete_argument_balise(1, $p);

	if (!$_id) {
		$_id = champ_sql('id_document', $p);
	}
	
	$nom = $p->nom_champ;
	$code = generer_generer_url_arg('document_fichier', $p, $_id);
	$code = champ_sql($nom, $p, $code);
	$p->code = $code;
	if (!$p->etoile) {
		$p->code = "vider_url($code)";
	}
	$p->interdire_scripts = false;
	
	return $p;
}
