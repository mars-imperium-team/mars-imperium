<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'a_savoir' => 'À savoir',
	'en_bref' => 'En bref',
	'afficher_tout' => 'Afficher tout',
	'bibliographie' => 'Bibliographie',
	'citer' => 'Pour citer',
	'favoris' => 'Favoris',
	'haut' => 'Haut de page',
	'historique' => 'Historique',
	'masquer' => 'Masquer',
	'mots' => 'Mots-clés',
	'naviguer' => 'Naviguer',
	'parcours' => 'Parcours',
	'role_logo_compact' => 'Logo compact',
	'role_logo_unite' => 'Logo unité',
	'selection' => 'Sélection',
	'texte_deplier' => 'Lire le texte complet',
	'texte_replier' => 'Replier le texte',
	'titre_court' => 'Titre court',
	'voir_notice' => 'Voir la notice détaillée',
);
