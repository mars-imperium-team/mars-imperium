<?php

function recommandations_defaut(string $objet, int $id_objet, array $options = []) {
	include_spip('base/abstract_sql');
	include_spip('base/objets');
	
	// Algo par défaut Jaccard
	if (!isset($options['algo']) or !in_array($options['algo'], ['cosinus', 'jaccard'])) {
		$algo = 'jaccard';
	}
	else {
		$algo = $options['algo'];
	}
	
	/**
	Cosine Similarity
	=================
	
	SELECT 
			other_content.id_content AS other_content_id,
			SUM(target_content.ponderation * other_content.ponderation) 
				/ (SQRT(SUM(target_content.ponderation * target_content.ponderation)) 
				* SQRT(SUM(other_content.ponderation * other_content.ponderation))) AS cosine_similarity
	FROM 
			content_keywords AS target_content
	JOIN 
			content_keywords AS other_content 
			ON target_content.id_keyword = other_content.id_keyword
	WHERE 
			target_content.id_content = <target_content_id>
			AND other_content.id_content != <target_content_id>
	GROUP BY 
			other_content.id_content
	ORDER BY 
			cosine_similarity DESC;
	
	
	Jaccard Similarity
	==================
	
	WITH Intersect AS (
			SELECT 
					other_content.id_content AS other_content_id,
					SUM(LEAST(target_content.ponderation, other_content.ponderation)) AS intersection_sum
			FROM 
					content_keywords AS target_content
			JOIN 
					content_keywords AS other_content 
					ON target_content.id_keyword = other_content.id_keyword
			WHERE 
					target_content.id_content = <target_content_id>
					AND other_content.id_content != <target_content_id>
			GROUP BY 
					other_content.id_content
	),
	Union AS (
			SELECT 
					other_content.id_content AS other_content_id,
					SUM(GREATEST(target_content.ponderation, other_content.ponderation)) AS union_sum
			FROM 
					content_keywords AS target_content
			JOIN 
					content_keywords AS other_content 
					ON target_content.id_keyword = other_content.id_keyword
			WHERE 
					target_content.id_content = <target_content_id>
					AND other_content.id_content != <target_content_id>
			GROUP BY 
					other_content.id_content
	)
	SELECT 
			Intersect.other_content_id,
			Intersect.intersection_sum / Union.union_sum AS jaccard_similarity
	FROM 
			Intersect
	JOIN 
			Union
			ON Intersect.other_content_id = Union.other_content_id
	ORDER BY 
			jaccard_similarity DESC;

	*/
	$recommandations = [];
	
	$where_inclure = '1=1';
	if (
		isset($options['inclure_objets'])
		and is_array($options['inclure_objets'])
	) {
		$options['inclure_objets'] = array_map('objet_type', $options['inclure_objets']);
		$where_inclure = sql_in('reco.objet', $options['inclure_objets']);
	}
	
	// Est-ce qu'on demande les recommandations d'un unique objet précis ?
	$where_articles = [];
	if (
		isset($options['inclure_objets'])
		and count($options['inclure_objets']) == 1
		and $recommander_objet = reset($options['inclure_objets'])
	) {
		// Si c'est spécifiquement pour des articles il y a des options possibles en plus
		if ($recommander_objet == 'article') {
			// Seulement les articles d'un secteur
			if (isset($options['id_secteur']) and $options['id_secteur'] > 0) {
				$ids = sql_allfetsel('id_article', 'spip_articles', 'id_secteur = ' . intval($options['id_secteur']));
				$ids = array_column($ids, 'id_article');
				$where_articles[] = sql_in('reco.id_objet', $ids);
			}
			
			// Exclure les articles d'une rubrique
			if (isset($options['exclure_id_rubrique']) and $options['exclure_id_rubrique'] > 0) {
				$ids = sql_allfetsel('id_article', 'spip_articles', 'id_rubrique = ' . intval($options['exclure_id_rubrique']));
				$ids = array_column($ids, 'id_article');
				$where_articles[] = sql_in('reco.id_objet', $ids, 'not');
			}
		}
		
		$exclure_ids = $options['exclure_ids'] ?? [];
		if ($exclure_ids and is_array($exclure_ids)) {
			$where_articles[] = sql_in('reco.id_objet', $exclure_ids, 'not');
		}
	}
	$where_articles = $where_articles ? implode(' and ', $where_articles) : '1=1';
	
	if ($objet and $id_objet) {
		// Cosine Similarity
		// =================
		if ($algo == 'cosinus' and $liste = sql_allfetsel(
			[
				'reco.objet',
				'reco.id_objet',
				'sum(contenu.ponderation * reco.ponderation) / (sqrt(sum(contenu.ponderation * contenu.ponderation)) * sqrt(sum(reco.ponderation * reco.ponderation))) as similarite',
			],
			'spip_recommandations_attributs_liens as contenu join spip_recommandations_attributs_liens as reco on contenu.id_recommandations_attribut=reco.id_recommandations_attribut',
			[
				'contenu.objet = '.sql_quote($objet),
				'contenu.id_objet = '.intval($id_objet),
				'(contenu.objet != reco.objet OR contenu.id_objet != reco.id_objet)',
				$where_inclure,
				$where_articles,
			],
			'reco.objet, reco.id_objet',
			'similarite desc'
		)) {
			$recommandations = $liste;
		}
		
		// Jaccard Similarity
		// ==================
		if (
			$algo == 'jaccard'
			and $result = sql_query(
				'with
					intersections as (
						select reco.objet as objet, reco.id_objet as id_objet, sum(least(contenu.ponderation, reco.ponderation)) as somme
						from spip_recommandations_attributs_liens as contenu
						join spip_recommandations_attributs_liens as reco on contenu.id_recommandations_attribut=reco.id_recommandations_attribut
						where contenu.objet = ' . sql_quote($objet) . ' and contenu.id_objet = ' .intval($id_objet) . ' and (contenu.objet != reco.objet OR contenu.id_objet != reco.id_objet) and ' . $where_inclure . ' and ' . $where_articles . '
						group by reco.objet, reco.id_objet
					),
					unions as (
						select reco.objet as objet, reco.id_objet as id_objet, sum(greatest(contenu.ponderation, reco.ponderation)) as somme
						from spip_recommandations_attributs_liens as contenu
						join spip_recommandations_attributs_liens as reco on contenu.id_recommandations_attribut=reco.id_recommandations_attribut
						where contenu.objet = ' . sql_quote($objet) . ' and contenu.id_objet = ' .intval($id_objet) . ' and (contenu.objet != reco.objet OR contenu.id_objet != reco.id_objet) and ' . $where_inclure . ' and ' . $where_articles . '
						group by reco.objet, reco.id_objet
					)
				select intersections.objet, intersections.id_objet, ((intersections.somme * 1.0) / (unions.somme * 1.0)) as similarite
				from intersections
				join unions on intersections.objet=unions.objet and intersections.id_objet=unions.id_objet
				order by similarite desc'
			)
			and $liste = sql_fetch_all($result)
		) {
			$recommandations = $liste;
		}
	}
	
	return $recommandations;
}

/**
 * Indexation des contenus de Mars Imperium : 
 * - on cherche les attributs intéressants du logo
 * - puis tout ce qui pourrait être lié au logo directement dans Omeka si ça existe (pour les docs internes aux vidéos)
 * - puis tous les documents joints "normaux" (role = document)
 */
function recommandations_defaut_indexer(string $objet, int $id_objet) {
	include_spip('base/abstract_sql');
	include_spip('inc/recommandations');
	$attributs = [];
	
	
	// S'il y a un logo OU le document directement
	$chercher_logo = charger_fonction('chercher_logo', 'inc');
	if (
		(
			($objet == 'document' and $omeka_data = sql_getfetsel('omeka_data', 'spip_documents', 'id_document = '.$id_objet))
			or ($objet != 'document' and $logo = $chercher_logo($id_objet, id_table_objet($objet)) and $omeka_data = $logo[5]['omeka_data'] ?? '')
		)
		and $omeka_data = json_decode($omeka_data, true)
	) {
		$attributs = lister_attributs_omeka($attributs, $omeka_data, 'logo');
		
		// On cherche s'il y a des items Omeka liés à l'intérieur (pour les vidéos avec des docs dedans)
		if (isset($omeka_data['dcterms:references'])) {
			$ids = [];
			foreach ($omeka_data['dcterms:references'] as $reference) {
				$ids[] = $reference['@id'];
			}
			$ids = array_filter($ids);
			if ($docs = sql_allfetsel('omeka_data', 'spip_documents', sql_in('omeka', $ids))) {
				foreach ($docs as $doc) {
					$omeka_data = json_decode($doc['omeka_data'], true);
					$attributs = lister_attributs_omeka($attributs, $omeka_data, 'reference');
				}
			}
		}
	}
	
	// Les documents joints normaux
	if ($docs = sql_allfetsel(
		'omeka_data',
		'spip_documents as d join spip_documents_liens as l on l.id_document=d.id_document',
		['l.objet='.sql_quote($objet), 'l.id_objet='.intval($id_objet), 'l.role="document"']
	)) {
		foreach ($docs as $doc) {
			if ($doc['omeka_data'] and $omeka_data = json_decode($doc['omeka_data'], true)) {
				$attributs = lister_attributs_omeka($attributs, $omeka_data, 'document');
			}
		}
	}
	
	// Maintenant on peut transformer les occurences trouvées en pondération
	$ponderations = [
		'logo' => 10,
		'reference' => 5,
		'document' => 1,
	];
	foreach ($attributs as $id => $attribut) {
		$ponderation = 0;
		
		foreach ($attribut['occurences'] as $type => $nb) {
			if ($nb > 0 and isset($ponderations[$type])) {
				$ponderation += $ponderations[$type];
			}
		}
		
		if ($ponderation) {
			$attributs[$id]['ponderation'] = $ponderation;
			// Plus besoin des occurences
			unset($attributs[$id]['occurences']);
			// On balance ça dans la base
			recommandations_objet_ajouter_attribut($objet, $id_objet, $attributs[$id]);
		}
		// Sinon pas cet attribut
		else {
			unset($attributs[$id]);
		}
	}
	
	return $attributs;
}

/**
 *  Liste les attributs possibles dans un item d'Omeka (subjects, geo, etc)
 * 
 * @param array $attributs Liste des attributs déjà trouvés
 * @param array $item Tableau du JSON d'un item Omeka
 * @param string $occurrence Identifiant d'à quoi correspond les attributs trouvés dans cette recherche
 */
function lister_attributs_omeka(array $attributs, array $item, string $occurence) {
	$unique = [];
	
	// Les mots-clés
	if (isset($item['dcterms:subject'])) {
		foreach ($item['dcterms:subject'] as $attribut) {
			if (isset($attribut['@id'])) {
				$attributs[$attribut['@id']] = $attributs[$attribut['@id']] ?? [];
				$attributs[$attribut['@id']]['identifiant'] = $attribut['@id'];
				$attributs[$attribut['@id']]['titre'] = $attributs[$attribut['@id']]['titre'] ?? $attribut['o:label'] ?? $attribut['@id'];
				$attributs[$attribut['@id']]['occurences'] = $attributs[$attribut['@id']]['occurences'] ?? [];
				$unique[$attribut['@id']] = isset($unique[$attribut['@id']]) ? 0 : 1;
				$attributs[$attribut['@id']]['occurences'][$occurence] = ($attributs[$attribut['@id']]['occurences'][$occurence] ?? 0) + $unique[$attribut['@id']];
			}
		}
	}
	
	// La géo
	foreach (['geo:location', 'dcterms:spatial'] as $champ_geo) {
		if (isset($item[$champ_geo])) {
			foreach ($item[$champ_geo] as $cle => $attribut) {
				if (isset($attribut['@id'])) {
					$attributs[$attribut['@id']] = $attributs[$attribut['@id']] ?? [];
					$attributs[$attribut['@id']]['identifiant'] = $attribut['@id'];
					$attributs[$attribut['@id']]['titre'] = $attributs[$attribut['@id']]['titre'] ?? $attribut['o:label'] ?? $attribut['@id'];
					$attributs[$attribut['@id']]['occurences'] = $attributs[$attribut['@id']]['occurences'] ?? [];
					$unique[$attribut['@id']] = isset($unique[$attribut['@id']]) ? 0 : 1;
					$attributs[$attribut['@id']]['occurences'][$occurence] = ($attributs[$attribut['@id']]['occurences'][$occurence] ?? 0) + $unique[$attribut['@id']];
				}
			}
		}
	}
	
	return $attributs;
}
