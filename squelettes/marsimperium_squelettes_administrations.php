<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) return;

include_spip('inc/cextras');
include_spip('marsimperium_squelettes_pipelines');
	
function marsimperium_squelettes_upgrade($nom_meta_base_version, $version_cible) {
	$maj = array();
	cextras_api_upgrade(marsimperium_squelettes_declarer_champs_extras(), $maj['create']);
	cextras_api_upgrade(marsimperium_squelettes_declarer_champs_extras(), $maj['1.2.0']);
	
	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

function marsimperium_squelettes_vider_tables($nom_meta_base_version) {
	include_spip('inc/meta');
	
  cextras_api_vider_tables(marsimperium_squelettes_declarer_champs_extras());
  effacer_meta($nom_meta_base_version);
}
