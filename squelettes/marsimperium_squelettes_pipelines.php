<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// Déclaration des menus utilisés (plugin menus)
function marsimperium_squelettes_menus_utiles($menus) {
	$menus['firstnav'] = 'Menu principal';
	$menus['social'] = 'Social';
	$menus['access'] = 'Accessibilité';
	$menus['footer'] = 'Pied de page';

	return $menus;
}

// Déclaration des pages utilisées (plugin pages)
function marsimperium_squelettes_pages_uniques_utiles($pages) {
	$pages['manuel'] = 'Manuel du site';
	$pages['404'] = 'Erreur 404 : contenu absent';
	$pages['403'] = 'Erreur 403 : accès interdit';

	return $pages;
}

/**
 * Modifier le résultat de la compilation des squelettes des formulaires
 *
 * - Login : supprimer cette saleté d'autofocus
 * - Recherche : ajouter un placeholder
 *
 * @param array $flux
 * @return array
 **/
function marsimperium_squelettes_formulaire_fond($flux) {

	// Login : enlever autofocus
	if ($flux['args']['form'] == 'login'
		and strpos($flux['data'], 'autofocus')
	) {
		$cherche = '/autofocus\s?=\s?[\"\']autofocus[\"\']/i';
		$remplace = '';
		$flux['data'] = preg_replace($cherche, $remplace, $flux['data']);
	}

	// Recherche : ajouter placeholder
	if ($flux['args']['form'] == 'recherche') {
		$texte_rechercher = _T('info_rechercher');
		$cherche = '/name=[\"\']recherche[\"\']/i';
		$remplace = 'name="recherche" placeholder="'.$texte_rechercher.'"';
		$flux['data'] = preg_replace($cherche, $remplace, $flux['data']);
	}

	return $flux;
}

/**
 * Machiner des trucs après des binz
 *
 * Modèle <ligne> : fusionner les <ul class="spip_documents_ligne"> en une seule ligne.
 * 
 * @param string $txt
 * @return string
 */
function marsimperium_squelettes_post_echappe_html_propre($txt) {

	$txt = preg_replace ("/<\/[ud]l>[\r\n\ ]*<[ud]l class=\"[^\"]*spip_documents[_ ]ligne[^\"]*\">/", "", $txt);

	return $txt;
}

/**
 * Méthodes d'upload : supprimer des possibilités pour les rédacs
 *
 * @param string $txt
 * @return string
 */
function marsimperium_squelettes_medias_methodes_upload($flux) {
	if (!autoriser('configurer')) {
		unset($flux['data']['upload']);
		unset($flux['data']['mediatheque']);
		unset($flux['data']['distant']);
	}
	
	return $flux;
}

/**
 * Ajouter le calcul du sommaire automatique sur les textes d'article
 * @param $interfaces
 * @return mixed
 */
function marsimperium_squelettes_declarer_tables_interfaces($interfaces) {
	$traitement = $interfaces['table_des_traitements']['TEXTE'][0];
	if (isset($interfaces['table_des_traitements']['TEXTE']['itineraires_etapes'])) {
		$traitement = $interfaces['table_des_traitements']['TEXTE']['itineraires_etapes'];
	}

	$traitement = str_replace('propre(', 'sommaire_propre(', $traitement);
	$interfaces['table_des_traitements']['TEXTE']['itineraires_etapes'] = $traitement;

	return $interfaces;
}

/**
 * Ajouter les champs de bibliographies
 * 
 */
function marsimperium_squelettes_declarer_champs_extras($champs=[]) {
	if (!is_array($champs)) {
		$champs = [];
	}
	
	$champs['spip_rubriques']['titre_court'] = array(
		'saisie' => 'input',
		'options' => array(
			'nom' => 'titre_court',
			'label' => _T('marsimperium:titre_court'),
			'sql' => "text not null default ''",
			'traitements' => '_TRAITEMENT_TYPO',
		),
	);
	$champs['spip_rubriques']['soustitre'] = array(
		'saisie' => 'input',
		'options' => array(
			'nom' => 'soustitre',
			'label' => _T('ecrire:texte_sous_titre'),
			'sql' => "text not null default ''",
			'traitements' => '_TRAITEMENT_TYPO',
		),
	);
	$champs['spip_articles']['bibliographie'] = array(
		'saisie' => 'textarea',
		'options' => array(
			'nom' => 'bibliographie',
			'label' => _T('marsimperium:bibliographie'),
			'rows' => 10,
			'conteneur_class' => 'pleine_largeur',
			'inserer_barre' => 'edition',
			'previsualisation' => 'oui',
			'sql' => "text not null default ''",
			'traitements' => '_TRAITEMENT_RACCOURCIS',
		),
	);
	$champs['spip_itineraires_etapes']['bibliographie'] = array(
		'saisie' => 'textarea',
		'options' => array(
			'nom' => 'bibliographie',
			'label' => _T('marsimperium:bibliographie'),
			'rows' => 10,
			'conteneur_class' => 'pleine_largeur',
			'inserer_barre' => 'edition',
			'previsualisation' => 'oui',
			'sql' => "text not null default ''",
			'traitements' => '_TRAITEMENT_RACCOURCIS',
		),
	);
	
	return $champs;
}

function marsimperium_squelettes_declarer_tables_objets_sql($flux) {
	// Tous les statuts de brouillon sont visibles de base aussi
	$flux['spip_articles']['statut'][0]['publie'] = 'publie,prop,prepa';
	$flux['spip_documents']['statut'][0]['publie'] = 'publie,prop,prepa';
	$flux['spip_itineraires']['statut'][0]['publie'] = 'publie,prop,prepa';
	//~ $flux['spip_itineraires_etapes']['statut'][0]['publie'] = 'publie,prop,prepa';
	
	// Ajout de rôles pour les docs de rubriques
	$flux['spip_documents'] = array_merge_recursive(
		$flux['spip_documents'],
		[
      'roles_titres' => [
        'logo_compact' => 'marsimperium:role_logo_compact',
        'logo_unite'  => 'marsimperium:role_logo_unite',
      ],
      'roles_objets' => [
        'rubriques' => [
          'choix' => ['logo', 'logo_compact', 'logo_unite', 'document'],
          'defaut' => 'document',
          'principaux' => ['logo', 'logo_survol', 'logo_compact', 'logo_unite'],
        ],
      ],
    ]
	);
	
	// Les documents ont une page dédiée
	$flux['spip_documents']['page'] = 'document';
	
	return $flux;
}

/**
 * Une variante d'album utilisant le slider Slick
 */
function marsimperium_squelettes_albums_decrire_dispositions($flux) {
	include_spip('inc/config');
	$largeur_config = lire_config('albums/img_largeur', null);
	$hauteur_config = lire_config('albums/img_hauteur', null);
	
	unset($flux['data']['line']);
	unset($flux['data']['list']);
	unset($flux['data']['browse']);
	
	$flux['data']['slick'] = [
		'modele_album' => 'album_slick',
		'largeur' => $largeur_config,
		'hauteur' => $hauteur_config,
		'images' => true,
	];
	
	return $flux;
}

/**
 * Modifier les modèles de document pour faire un lien vers une visionneuse plus complète
 */
function marsimperium_squelettes_recuperer_fond($flux) {
	if (!test_espace_prive() and in_array($flux['args']['fond'], ['modeles/image', 'modeles/doc', 'modeles/file'])) {
		// Si on trouve bien un document et qu'il n'y a pas de lien personnalisé
		$id_document = $flux['args']['contexte']['id'] ?? $flux['args']['contexte']['id_document'] ?? 0;
		if ($id_document && !($flux['args']['contexte']['lien'] ?? '')) {
			// On cherche s'il y a un lien mediabox généré
			if (preg_match('%<a[^>]*(spip_doc_lien)[^>]*>%', $flux['data']['texte'], $lien)) {
				include_spip('inc/filtres');
				$lien = $lien[0]; // La chaine exacte du lien à remplacer plus tard
				
				$lien_modif = vider_attribut($lien, 'type');
				//~ $lien_modif = inserer_attribut($lien_modif, 'href', generer_url_public('visionneuse', 'id_document='.$id_document));
				$lien_modif = inserer_attribut($lien_modif, 'data-box-sidebar', 'bottom');
				$lien_modif = inserer_attribut($lien_modif, 'data-box-type', 'ajax');
				$lien_modif = inserer_attribut($lien_modif, 'data-box-class', 'box_visionneuse');
				$lien_modif = ajouter_class($lien_modif, 'popin');
				
				// On échange
				$flux['data']['texte'] = str_replace($lien, $lien_modif, $flux['data']['texte']);
			}
		}
	}
	
	// Pour rajouter un picto pour le secteur
	if (in_array($flux['args']['fond'], ['inclure/previews/article', 'inclure/previews/document', 'inclure/previews/itineraires_etape'])) {
		$id_secteur = 0;
		
		// Si c'est pour un article
		if ($flux['args']['fond'] == 'inclure/previews/article') {
			$id_article = $flux['args']['contexte']['id_article'];
			$id_secteur = sql_getfetsel('id_secteur', 'spip_articles', 'id_article = '.$id_article);
		}
		// Si c'est pour un document
		if ($flux['args']['fond'] == 'inclure/previews/document') {
			$id_secteur = sql_getfetsel('id_rubrique', 'spip_rubriques', ['id_parent=0', 'composition="mediatheque"']);
		}
		// Si c'est pour une étape
		if ($flux['args']['fond'] == 'inclure/previews/itineraires_etape') {
			$id_secteur = sql_getfetsel('id_rubrique', 'spip_rubriques', ['id_parent=0', 'composition="balades"']);
		}
		
		// Si on a trouvé un secteur à afficher
		if ($id_secteur) {
			include_spip('inc/documents');
			include_spip('inc/filtres');
			
			$titre_secteur = generer_objet_info($id_secteur, 'rubrique', 'titre');
			// On cherche le bon logo associé
			$logo = sql_getfetsel(
				'fichier',
				'spip_documents as d join spip_documents_liens as l on l.id_document=d.id_document',
				['l.role="logo_unite"', 'objet="rubrique"', 'id_objet='.$id_secteur]
			);
			$logo = get_spip_doc($logo);
			$logo = wrap(filtrer('balise_img_svg', $logo, $titre_secteur), '<div class="preview__plateforme">');
			
			$flux['data']['texte'] = preg_replace('|<div[^>]*preview__body[^>]*>|i', '$0'.$logo, $flux['data']['texte']);
		}
	}
	
	return $flux;
}
