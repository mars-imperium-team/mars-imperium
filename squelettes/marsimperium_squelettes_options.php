<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// Activer HTML5 depuis le squelette
$GLOBALS['meta']['version_html_max'] = 'html5';

// Déclaration des blocs pour Z (plugin z-core)
if (!isset($GLOBALS['z_blocs'])) {
	$GLOBALS['z_blocs'] = array(
		'main',
		'aside',
		//~ 'extra',
		'head',
		'head_js',
		'header',
		'footer',
		//~ 'breadcrumb',
	);
}

// Désactiver le portfolio
// (toutes les images au dessus de cette taille sont déposées dans le portfolio, donc aucune si on met une grande taille)
define('_LARGEUR_MODE_IMAGE', 99999);

// Plus d'images cliquables pour agrandir
define('_IMAGE_TAILLE_MINI_AUTOLIEN', 200);

// Toujours chercher les documents dans "descriptif" aussi
$GLOBALS['medias_liste_champs'][] = 'descriptif';

define('SOMMAIRE_AUTO_OFF', true);
