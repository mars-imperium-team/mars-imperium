;jQuery(function($) {

	// Les breakpoints (cf. _grid-settings.scss)
	var screen_tablet   = 480;
	var screen_desktop  = 960;
	var screen_large    = 1200;


	/**
	 * =============
	 * Les fonctions
	 * =============
	 */

	/**
	 * Menu d'accessibilité
	 */
	function gerer_access() {
		// Apparition / disparition
		$( '#access' ).focusin(function(e){
			$(this).addClass( 'actif' );
		});
		$( '#access' ).focusout(function(e){
			$(this).removeClass( 'actif' );
		});
	}

	/**
	 * Gestion du responsive
	 */
	function gerer_responsive() {
		var screen = $(window).width();
		if (screen < screen_tablet) {
		} else if (screen >= screen_tablet && screen < screen_desktop) {
		} else if (screen >= screen_desktop && screen < screen_large) {
		} else if (screen >= screen_large) {
		}
	}
	
	// Comportement du bouton de menu
	//~ var priorityNavButtonClick = function (button, _this, settings) {
		//~ $('body').css('overflow', 'hidden');
		//~ $('.page').addClass('behind');
		//~ $('.nav_full')
			//~ .removeClass('nav_full_closed')
			//~ .attr('aria-hidden', 'false')
			//~ .find('.nav__close')
			//~ .focus();
		
		//~ $(button).toggleClass('is-open');
		//~ return false;
	//~ }

	/**
	 * ================================================
	 * Lancement des fonctions une fois la page chargée
	 * ================================================
	 */

	$(function() {
		// 5rem
		$.spip.positionner_marge = 5 * (16 + 0.4 * Math.max(document.documentElement.clientWidth, window.innerWidth || 0) / 100);
		gerer_access();
		
		// Poser une classe sur le header quand on scroll
		var header = document.getElementById('header');
		var header_height = header.offsetHeight;
		window.addEventListener('scroll', function () {
			if (window.scrollY > 0) {
					if (!header.classList.contains('is_stuck')) {
						header.classList.add('is_stuck');
						setTimeout(function() {
							Array.from(document.getElementsByClassName('fullcontent__menu')).forEach(
								(menu) => menu.style.top = header.offsetHeight+'px'
							);
						}, 500);
					}
			} else {
					header.classList.remove('is_stuck');
					setTimeout(function() {
						Array.from(document.getElementsByClassName('fullcontent__menu')).forEach(
							(menu) => menu.style.top = header.offsetHeight+'px'
						);
					}, 500);
			}
    });
		
		//~ // Priority sur le menu principal
		//~ if ($('.priority-nav').length === 0) {
			//~ var pnav = priorityNav.init({
				//~ mainNavWrapper: '.header .menu_firstnav_main',
				//~ mainNav: '.menu-items',
				//~ breakPoint: screen_tablet,
				//~ navDropdownLabel: 'Plus',
				//~ navDropdownBreakpointLabel: 'Menu',
				//~ buttonHandler: priorityNavButtonClick,
			//~ });
		//~ }
		
		//~ setTimeout(function() {
			//~ $('#header').each(function() {
				//~ var me = $(this);
				//~ var hauteur = me.height();
				
				//~ $('body').css('padding-top', hauteur);
				//~ me.addClass('header_fixed');
				
				//~ // Sticky sur tout le header
				//~ me.stick_in_parent({
					//~ parent: '.page',
					//~ spacer: false,
				//~ });
			//~ });
		//~ }, 1000);
		
		/**
		 * Lors du changement de taille du navigateur.
		 * On relance la fonction à chaque changement de la dimension de la fenêtre,
		 * avec un timeout pour ne pas gréver la perf.
		 */
		/*
		var responsiveTimer;
		$(window).resize(function () {
			clearTimeout(responsiveTimer);
			responsiveTimer = setTimeout(gerer_responsive, 150);
		});
		*/
	});
	
	// Pliage des textes
	$('.fullcontent .text_fold').each(function () {
		var bloc = $(this);
		var texte = bloc.find('.texte');
		
		// Au démarrage le texte replié
		texte.addClass('folded');
		
		// On ajoute un bouton à la fin
		bloc.append(
			$('<button class="button_folding" aria-expanded="false" aria-controls="text_fold">Lire le texte complet</button>')
				.click(function() {
					var button = $(this);
					
					if (button.attr('aria-expanded') == 'false') {
						button.attr('aria-expanded', 'true').text('Replier le texte');
						texte.removeClass('folded');
					}
					else {
						button.attr('aria-expanded', 'false').text('Lire le texte complet');
						texte.addClass('folded').positionner();
					}
				})
		);
	});
	// Le sommaire doit toujours déplier le texte complet aussi
	$('#sommaire a').click(function() {
		var href = $(this).attr('href');
		var texte = $('.fullcontent .text_fold .texte');
		var button = $('.fullcontent .text_fold .button_folding');
		
		if (texte.is('.folded')) {
			button.click();
		}
		
		$(href).positionner(true);
	});
	
	// Masquer l'avant-propos quand on lance la lecture des vidéos
	$('.composition_webdoc .fullcontent__media').each(function() {
		var media = $(this);
		
		media.find('.oe-video').click(function() {
			media.find('.descriptif').hide();
		});
	});
	
	// Dans les rubriques sliders sur les listes
	$('.page_rubrique .list:not(.list_documents) .list__items').slick({
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 1,
		swipeToSlide: true,
		centerMode: true,
		responsive: [
			{
				breakpoint: screen_large,
				settings: {
					slidesToShow: 2,
				}
			},
			{
				breakpoint: screen_tablet,
				settings: {
					slidesToShow: 1,
				}
			},
		]
	});
});
