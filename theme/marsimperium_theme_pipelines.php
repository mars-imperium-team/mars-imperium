<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Déclarer les couleurs à utiliser
 */
function marsimperium_theme_cssvar($flux) {
	// Hors contexte, pour toutes les pages
	if (empty($flux['args'])) {
		// Bleu
		$flux['data'][':root']['--color-primary'] = '#006B8F';
		// Bleu marine
		$flux['data'][':root']['--color-secondary'] = '#043244';
		// Neutre
		$flux['data'][':root']['--color-neutre'] = '#9D9D9C';
		
		$flux['data'][':root']['--mediabox-sidebar-width'] = '100vw !important';
		$flux['data'][':root']['--mediabox-sidebar-height'] = '100vh !important';
	}
	
	return $flux;
}
